#include "../encabezados/player.h"

Player::Player(int _xIndex, int _yIndex, int width, int height, Color _color, Texture2D _sprite) : Tile(_xIndex, _yIndex, width, height, _color, _sprite)
{		
	moveStatus = Movement::none;
	oldMoveStatus = Movement::none;
	SetMoveUpKey(KEY_W);
	SetMoveDownKey(KEY_S);
	SetMoveRightKey(KEY_D);
	SetMoveLeftKey(KEY_A);
}
void Player::Input()
{
	if (IsKeyPressed(moveUpKey))
	{		
		SetMoveStatus(Movement::up);
		SetOldMoveStatus(Movement::up);
	}
	else if (IsKeyPressed(moveDownKey))
	{		
		SetMoveStatus(Movement::down);
		SetOldMoveStatus(Movement::down);
	}
	else if (IsKeyPressed(moveRightKey))
	{		
		SetMoveStatus(Movement::right);
		SetOldMoveStatus(Movement::right);
	}
	else if (IsKeyPressed(moveLeftKey))
	{		
		SetMoveStatus(Movement::left);
		SetOldMoveStatus(Movement::left);
	}
	else
	{
		SetMoveStatus(Movement::none);
		SetOldMoveStatus(Movement::none);
	}
}
void Player::WallColision(Tile* wall)
{
	switch (GetMoveStatus())
	{
	case Movement::up:

		if (GetXIndex() == wall->GetXIndex() && GetYIndex() - 1 == wall->GetYIndex())
		{
			SetMoveStatus(Movement::none);
		}

		break;
	case Movement::down:

		if (GetXIndex() == wall->GetXIndex() && GetYIndex() + 1 == wall->GetYIndex())
		{
			SetMoveStatus(Movement::none);
		}

		break;
	case Movement::right:

		if (GetYIndex() == wall->GetYIndex() && GetXIndex() + 1 == wall->GetXIndex())
		{
			SetMoveStatus(Movement::none);
		}

		break;
	case Movement::left:

		if (GetYIndex() == wall->GetYIndex() && GetXIndex() - 1 == wall->GetXIndex())
		{
			SetMoveStatus(Movement::none);
		}

		break;
	case Movement::none:
		break;
	}
}
void Player::Update(Texture2D frontSprite, Texture2D backSprite, Texture2D rightSprite, Texture2D leftSprite)
{
	switch (GetMoveStatus())
	{
	case Movement::up:
		SetYIndex(GetYIndex() - 1);
		SetSprite(backSprite);
		break;
	case Movement::down:
		SetYIndex(GetYIndex() + 1);
		SetSprite(frontSprite);
		break;
	case Movement::right:
		SetXIndex(GetXIndex() + 1);
		SetSprite(rightSprite);
		break;
	case Movement::left:
		SetXIndex(GetXIndex() - 1);
		SetSprite(leftSprite);
		break;
	case Movement::none:
		break;
	}

	switch (GetOldMoveStatus())
	{
	case Movement::up:		
		SetSprite(backSprite);
		break;
	case Movement::down:		
		SetSprite(frontSprite);
		break;
	case Movement::right:		
		SetSprite(rightSprite);
		break;
	case Movement::left:		
		SetSprite(leftSprite);
		break;
	case Movement::none:
		break;
	}

	CheckIfSpriteMatchSize();
	SetPosition(GetXIndex(), GetYIndex());
}
void Player::Draw()
{	
	Tile::Draw();
}

void Player::SetMoveStatus(Movement status)
{
	moveStatus = status;
}
void Player::SetOldMoveStatus(Movement status)
{
	oldMoveStatus = status;
}
void Player::SetMoveUpKey(int key)
{
	moveUpKey = key;
}
void Player::SetMoveDownKey(int key)
{
	moveDownKey = key;
}
void Player::SetMoveRightKey(int key)
{
	moveRightKey = key;
}
void Player::SetMoveLeftKey(int key)
{
	moveLeftKey = key;
}

Movement Player::GetMoveStatus()
{
	return moveStatus;
}
Movement Player::GetOldMoveStatus()
{
	return oldMoveStatus;
}
int Player::GetMoveUpKey()
{
	return moveUpKey;
}
int Player::GetMoveDownKey()
{
	return moveDownKey;
}
int Player::GetMoveRightKey()
{
	return moveRightKey;
}
int Player::GetMoveLeftKey()
{
	return moveLeftKey;
}
int Player::GetXIndex()
{
	return Tile::GetXIndex();
}
int Player::GetYIndex()
{
	return Tile::GetYIndex();
}