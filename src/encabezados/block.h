#ifndef BLOCK_H
#define BLOCK_H

#include "tile.h"

class Block : Tile
{
private:
	Vector2 destination;
public:
	Block(Vector2 _destination, int xIndex, int yIndex, int width, int height, Color _color, Texture2D _sprite);
	void Draw();
	void DrawDestination(Texture2D sprite);

	void SetPosition(int _xIndex, int _yIndex);
	void SetXIndex(float _xIndex);
	void SetYIndex(float _yIndex);
	void SetDestination(Vector2 _destination);	

	Vector2 GetDestination();	
	int GetXIndex();
	int GetYIndex();

	bool HasBlockReachedDestination();	
};

#endif

