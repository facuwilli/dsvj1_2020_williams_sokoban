#include "../encabezados/tile.h"

Tile::Tile(float _xIndex, float _yIndex, int width, int height, Color _color, Texture2D _sprite)
{
	SetWidth(width);
	SetHeight(height);
	SetYIndex(_yIndex);
	SetXIndex(_xIndex);
	SetPosition(_xIndex, _yIndex);
	SetColor(_color);
	SetSprite(_sprite);
}
void Tile::Draw()
{	
	DrawTexture(sprite, body.x, body.y, color);
}

void Tile::SetBody(int x, int y, int width, int height)
{
	SetX(x);
	SetX(y);
	SetWidth(width);
	SetHeight(height);
}
void Tile::SetX(int x)
{
	body.x = x;	
}
void Tile::SetY(int y)
{	
	body.y = y;	
}
void Tile::SetWidth(int width)
{	
	body.width = width;	
}
void Tile::SetHeight(int height) 
{	
	body.height = height;
}
void Tile::SetPosition(int _xIndex, int _yIndex)
{
	SetX(GetWidth() * (_xIndex));
	SetY(GetHeight() * (_yIndex));
}
void Tile::SetColor(Color _color)
{
	color = _color;
}
void Tile::SetXIndex(float _xIndex)
{
	if (_xIndex >= 0 && _xIndex < maxCol)
	{
		index.x = _xIndex;
	}
}
void Tile::SetYIndex(float _yIndex)
{
	if (_yIndex >= 0 && _yIndex < maxFil)
	{
		index.y = _yIndex;
	}
}
void Tile::SetSprite(Texture2D _sprite)
{
	sprite = _sprite;
}
void Tile::SetSpriteWidth(int width)
{
	sprite.width = width;
}
void Tile::SetSpriteHeight(int height)
{
	sprite.height = height;
}
void Tile::CheckIfSpriteMatchSize()
{
	if (GetSpriteWidth() != GetWidth())
	{
		SetSpriteWidth(GetWidth());		
	}

	if(GetSpriteHeight() != GetHeight())
	{
		SetSpriteHeight(GetHeight());
	}
}

Rectangle Tile::GetBody() 
{
	return body;
}
int Tile::GetX()
{
	return body.x;
}
int Tile::GetY() 
{
	return body.y;
}
int Tile::GetWidth()
{
	return body.width;
}
Color Tile::GetColor()
{
	return color;
}
int Tile::GetHeight()
{
	return body.height;
}
int Tile::GetXPosition(int _xIndex)
{
	return GetWidth() * _xIndex;
}
int Tile::GetYPosition(int _yIndex)
{
	return GetHeight() * _yIndex;
}
int Tile::GetXIndex()
{
	return static_cast<int>(index.x);
}
int Tile::GetYIndex()
{
	return static_cast<int>(index.y);
}
Texture2D Tile::GetSprite()
{
	return sprite;
}
int Tile::GetSpriteWidth()
{
	return sprite.width;
}
int Tile::GetSpriteHeight()
{
	return sprite.height;
}
