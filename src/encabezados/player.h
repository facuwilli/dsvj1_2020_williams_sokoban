#ifndef PLAYER_H
#define PLAYER_H

#include "tile.h"
#include "block.h"

enum class Movement {up, down, right, left, none};

class Player : Tile
{
private:	
	Movement moveStatus;	
	Movement oldMoveStatus;
	int moveUpKey;
	int moveDownKey;
	int moveRightKey;
	int moveLeftKey;
public:
	Player(int xIndex, int yIndex, int width, int height, Color _color, Texture2D _sprite);		
	void Input();	
	void WallColision(Tile* wall);
	void Update(Texture2D frontSprite, Texture2D backSprite, Texture2D rightSprite, Texture2D leftSprite);
	void Draw();

	void SetMoveStatus(Movement status);	
	void SetOldMoveStatus(Movement status);
	void SetMoveUpKey(int key);
	void SetMoveDownKey(int key);
	void SetMoveRightKey(int key);
	void SetMoveLeftKey(int key);

	Movement GetMoveStatus();
	Movement GetOldMoveStatus();
	int GetMoveUpKey();
	int GetMoveDownKey();
	int GetMoveRightKey();
	int GetMoveLeftKey();	
	int GetXIndex();
	int GetYIndex();
};

#endif
