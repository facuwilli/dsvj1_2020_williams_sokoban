#ifndef TILE_H
#define TILE_H

#include <iostream>
#include "raylib.h"
#include "const_vars.h"

using namespace std;

class Tile
{
private:
	Rectangle body;
	Color color;	
	Vector2 index;
	Texture2D sprite;
public:
	Tile(float xIndex, float yIndex, int width, int height, Color _color, Texture2D _sprite);
	virtual void Draw();	
	
	void SetBody(int x, int y, int width, int height);
	void SetX(int x);
	void SetY(int y);
	void SetWidth(int width);
	void SetHeight(int height);
	virtual void SetPosition(int _xIndex, int _yIndex);
	void SetColor(Color _color);
	virtual void SetXIndex(float _xIndex);
	virtual void SetYIndex(float _yIndex);
	void SetSprite(Texture2D _sprite);
	void SetSpriteWidth(int width);
	void SetSpriteHeight(int height);
	void CheckIfSpriteMatchSize();
	
	Rectangle GetBody();
	int GetX();
	int GetY();
	int GetWidth();
	int GetHeight();
	int GetXPosition(int _xIndex);
	int GetYPosition(int _yIndex);
	Color GetColor();
	virtual int GetXIndex();
	virtual int GetYIndex();	
	Texture2D GetSprite();
	int GetSpriteWidth();
	int GetSpriteHeight();
};

#endif
