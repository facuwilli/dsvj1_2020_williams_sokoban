#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "player.h"
#include "const_vars.h"
#include "block.h"

class Game
{
private:	
	int windowsWidth;
	int windowsHeight;	
	int fps;	
	Color backgroundColor;
	Player* player;
	Rectangle map[maxFil][maxCol];	
	Texture2D playerTextures[maxSprites];
	Texture2D ground;
	Block* box;
	Texture2D boxTexture;
	Texture2D destination;
	Tile* wall;
	Texture2D wallTexture;
public:
	Game();
	~Game();
	void Init();
	void Input();
	void Update();
	void Draw();
	void Deinit();
	void Play();
	void SetMap();	
	void DrawMap();
	void ObjectsCollision();
};

#endif


