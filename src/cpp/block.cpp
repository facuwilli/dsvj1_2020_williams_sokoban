#include "../encabezados/block.h"

Block::Block(Vector2 _destination, int xIndex, int yIndex, int width, int height, Color _color, Texture2D _sprite): Tile(static_cast<float>(xIndex), static_cast<float>(yIndex), width, height, _color, _sprite)
{
	SetDestination(_destination);	
}
void Block::Draw()
{
	Tile::Draw(); 
}
void Block::DrawDestination(Texture2D sprite)
{
	DrawTexture(sprite, GetXPosition(static_cast<int>(destination.x)), GetYPosition(static_cast<int>(destination.y)), GetColor());
}

void Block::SetPosition(int _xIndex, int _yIndex)
{
	Tile::SetPosition(_xIndex, _yIndex);
}
void Block::SetXIndex(float _xIndex)
{
	Tile::SetXIndex(_xIndex);
}
void Block::SetYIndex(float _yIndex)
{
	Tile::SetYIndex(_yIndex);
}
void Block::SetDestination(Vector2 _destination)
{
	if((_destination.x >= 0 && _destination.x < maxCol) && (_destination.y >= 0 && _destination.y < maxFil))
	{
		destination = { _destination.x,_destination.y };
	}
}

Vector2 Block::GetDestination()
{
	return destination;
}
int Block::GetXIndex()
{
	return Tile::GetXIndex();
}
int Block::GetYIndex()
{
	return Tile::GetYIndex();
}

bool Block::HasBlockReachedDestination()
{
	if(GetXIndex() == static_cast<int>(GetDestination().x) && GetYIndex() == static_cast<int>(GetDestination().y))
	{
		return true;
	}	
	else
	{
		return false;
	}
}
