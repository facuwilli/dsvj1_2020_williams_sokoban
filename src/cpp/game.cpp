#include "../encabezados/game.h"

Game::Game()
{
	windowsWidth = 480;
	windowsHeight = 480;	
	fps = 60;
	backgroundColor = WHITE;
	player = NULL;
	box = NULL;
	wall = NULL;
	InitWindow(windowsWidth, windowsHeight, "SOKOBAN");
	SetTargetFPS(60);	
	SetMap();		
}
Game::~Game()
{
	CloseWindow();
}
void Game::Init()
{
	playerTextures[0] = LoadTexture("../res/textures/player/player_front.png");
	playerTextures[1] = LoadTexture("../res/textures/player/player_back.png");
	playerTextures[2] = LoadTexture("../res/textures/player/player_right.png");
	playerTextures[3] = LoadTexture("../res/textures/player/player_left.png");
	ground = LoadTexture("../res/textures/ground/ground.png");	
	boxTexture = LoadTexture("../res/textures/box/box.png");
	destination = LoadTexture("../res/textures/ground/destination.png");
	wallTexture = LoadTexture("../res/textures/wall/wall.png");

	player = new Player(0, 0, static_cast<int>(map[0][0].width), static_cast<int>(map[0][0].height), WHITE, playerTextures[0]);
	box = new Block({1,2}, 4, 2, static_cast<int>(map[2][4].width), static_cast<int>(map[2][4].height), WHITE, boxTexture);
	wall = new Tile(3, 3, static_cast<int>(map[3][3].width), static_cast<int>(map[3][3].height), WHITE, wallTexture);
}
void Game::Input()
{
	player->Input();
}
void Game::Update()
{
	player->WallColision(wall);	
	ObjectsCollision();
	player->Update(playerTextures[0], playerTextures[1], playerTextures[2], playerTextures[3]);	
}
void Game::Draw() 
{
	BeginDrawing();

	ClearBackground(backgroundColor);

	DrawMap();

	wall->Draw();
	box->DrawDestination(destination);
	box->Draw();
	player->Draw();
	
	
	EndDrawing();
}
void Game::Deinit()
{
	delete player;
	player = NULL;

	delete box;
	box = NULL;

	delete wall;
	wall = NULL;

	for(short i = 0; i < maxSprites; i++)
	{
		UnloadTexture(playerTextures[i]);
	}
	
	UnloadTexture(ground);
	UnloadTexture(boxTexture);
	UnloadTexture(destination);
	UnloadTexture(wallTexture);
}
void Game::Play()
{
	Init();

	while(!WindowShouldClose())
	{
		Input();
		Update();
		Draw();
	}

	Deinit();
}
void Game::SetMap()
{
	for(short y = 0; y < maxFil; y++)
	{
		for(short x = 0; x < maxCol; x++)
		{			
			map[y][x].width = windowsWidth / maxCol;
			map[y][x].height = windowsHeight / maxFil;
			map[y][x].x = map[y][x].width * x;
			map[y][x].y = map[y][x].height * y;
		}
	}
}
void Game::DrawMap()
{
	for (short y = 0; y < maxFil; y++)
	{
		for (short x = 0; x < maxCol; x++)
		{			
			if (ground.width != map[y][x].width) { ground.width = map[y][x].width; }
			if (ground.height != map[y][x].height) { ground.height = map[y][x].height; }
			DrawTexture(ground, map[y][x].x, map[y][x].y, WHITE);
		}
	}
}
void Game::ObjectsCollision()
{
	switch (player->GetMoveStatus())
	{
	case Movement::up:

		if (player->GetXIndex() == box->GetXIndex() && player->GetYIndex() - 1 == box->GetYIndex())
		{
			if (box->GetXIndex() == wall->GetXIndex() && box->GetYIndex() - 1 == wall->GetYIndex())
			{
				player->SetMoveStatus(Movement::none);
			}
			else
			{
				box->SetYIndex(box->GetYIndex() - 1);
				box->SetPosition(box->GetXIndex(), box->GetYIndex());
			}
		}

		break;
	case Movement::down:

		if (player->GetXIndex() == box->GetXIndex() && player->GetYIndex() + 1 == box->GetYIndex())
		{
			if (box->GetXIndex() == wall->GetXIndex() && box->GetYIndex() + 1 == wall->GetYIndex())
			{
				player->SetMoveStatus(Movement::none);
			}
			else
			{
				box->SetYIndex(box->GetYIndex() + 1);
				box->SetPosition(box->GetXIndex(), box->GetYIndex());
			}
		}

		break;
	case Movement::right:

		if (player->GetYIndex() == box->GetYIndex() && player->GetXIndex() + 1 == box->GetXIndex())
		{
			if (box->GetYIndex() == wall->GetYIndex() && box->GetXIndex() + 1 == wall->GetXIndex())
			{
				player->SetMoveStatus(Movement::none);
			}
			else
			{
				box->SetXIndex(box->GetXIndex() + 1);
				box->SetPosition(box->GetXIndex(), box->GetYIndex());
			}
		}

		break;
	case Movement::left:

		if (player->GetYIndex() == box->GetYIndex() && player->GetXIndex() - 1 == box->GetXIndex())
		{
			if (box->GetYIndex() == wall->GetYIndex() && box->GetXIndex() - 1 == wall->GetXIndex())
			{
				player->SetMoveStatus(Movement::none);
			}
			else
			{
				box->SetXIndex(box->GetXIndex() - 1);
				box->SetPosition(box->GetXIndex(), box->GetYIndex());
			}
		}

		break;
	case Movement::none:
		break;
	}
}